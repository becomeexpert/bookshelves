import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'bookshelves';
  constructor(){
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyBXLL9u_IP5u3LcdRcD-AmTyNGo9GtBbKc",
    authDomain: "booksheves-2df7f.firebaseapp.com",
    databaseURL: "https://booksheves-2df7f.firebaseio.com",
    projectId: "booksheves-2df7f",
    storageBucket: "gs://booksheves-2df7f.appspot.com",
    messagingSenderId: "131516464507"
  };
  firebase.initializeApp(config);
  }
 
}
