import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { promise } from 'protractor';
import { resolve } from 'dns';
import { reject } from 'q';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  createNewUser(email: string , password: string){
    return new Promise(
      (resolve,reject) => {
        firebase.auth().createUserWithEmailAndPassword(email,password).then(
          () => {
            resolve();
          },
          (error) =>{
            reject(error);
          }
        );
      }
    );
  }
  signinUser(email: string, password: string){
    return new Promise(
      (resolve,reject) =>{
        firebase.auth().signInAndRetrieveDataWithEmailAndPassword(email,password).then(
          () =>{
            resolve();
          },
          (error) =>{
            reject(error);
          }
          
        );
      }
    );
  }
  signOutUser(){
    firebase.auth().signOut();
  }
}
